// import { StatusBar } from 'expo-status-bar';
// import { StyleSheet, Text, View } from 'react-native';
import React, { useEffect, useState } from 'react';
import { View } from 'react-native';
import { DataTable } from 'react-native-paper';

const MovieList = () => {
  const [movies, setMovies] = useState([]);

  useEffect(() => {
    const fetchMovies = async () => {
      const url = 'https://imdb8.p.rapidapi.com/auto-complete?q=spiderman';
      const options = {
        method: 'GET',
        headers: {
          'X-RapidAPI-Key': 'd93b443fe5msh7b0b6b5d6eb1249p1d4150jsn5a1a0980347c',
          'X-RapidAPI-Host': 'imdb8.p.rapidapi.com',
        },
      };

      try {
        const response = await fetch(url, options);
        const result = await response.json();
        setMovies(result.d);
      } catch (error) {
        console.error(error);
      }
    };

    fetchMovies();
  }, []);

  return (
    <View>
      <DataTable>
        <DataTable.Header>
          <DataTable.Title>Película</DataTable.Title>
          <DataTable.Title>Actores</DataTable.Title>
        </DataTable.Header>

        {movies.map((movie) => (
  <DataTable.Row key={movie.id}>
    <DataTable.Cell>{movie.l}</DataTable.Cell>
    <DataTable.Cell>
      {Array.isArray(movie.s) ? (
        movie.s.map((actor) => (
          <View key={actor.id}>
            <DataTable.Cell>{actor.l}</DataTable.Cell>
          </View>
        ))
      ) : (
        <DataTable.Cell>No hay actores disponibles</DataTable.Cell>
      )}
    </DataTable.Cell>
  </DataTable.Row>
))}

      </DataTable>
    </View>
  );
};

export default MovieList;

// export default function App() {
//   return (
//     <View style={styles.container}>
//       <Text>Que pedo axel</Text>
//       <StatusBar style="auto" />
//     </View>
//   );
// }

// const styles = StyleSheet.create({
//   container: {
//     flex: 1,
//     backgroundColor: '#fff',
//     alignItems: 'center',
//     justifyContent: 'center',
//   },
// });
